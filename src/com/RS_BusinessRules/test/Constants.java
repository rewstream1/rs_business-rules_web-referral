package com.RS_BusinessRules.test;

public class Constants {
	

	public static String CONFIG="config";
	
	public static String DATA="Data";
	public static String DATA_START_COL="col";
	public static String DATA_SPLIT="\\|";
	public static final String DESCRIPTION = "Description";
	
	public static String KEYWORD="Keyword";
	public static String KEYWORD_PASS="PASS";
	public static String KEYWORD_FAIL="FAIL";
	public static String KEYWORD_SKIP="SKIP";
	
	
	public static String OBJECT="Object";
	
	public static Object POSITIVE_DATA="Y";
	
	public static String RUNMODE_YES="Y";
	public static String RUNMODE = "Runmode";
	public static Object RANDOM_VALUE="Random_Value";
	public static String RESULT="Result";
	public static String RefereeId="ID";
	public static final String SUITE_ID = "TSID";

	
	
	public static String TEST_SUITE_SHEET="Test Suite";
	public static String TEST_CASES_SHEET="Test Cases";
	public static String TEST_STEPS_SHEET="Test Steps";
	public static String TCID="TCID";
	public static String Test_Suite_ID="TSID";
	public static String Referrer="default@test";
	public static String Filename="default_filename";
	public static String AccountNo="1111";
	public static String Pin="11111";
	public static String Referee="test578@test337";
	public static String OldTab="old_tab";

	public static String AccountNoReferee="default@test";

	public static String MatchKey="MatchKeyDefault";

	public static String ActualText_Valid="DefaultText";

	public static String ActualText_Unknown="DefaultText";
	

	
	
	



}
