package com.RS_BusinessRules.test;
import static com.RS_BusinessRules.test.DriverScript.APP_LOGS;
import static com.RS_BusinessRules.test.DriverScript.CONFIG;
import static com.RS_BusinessRules.test.DriverScript.OR;



//import static com.qtpselenium.test.DriverScript.currentTestSuiteXLS;
//import static com.qtpselenium.test.DriverScript.currentTestCaseName;
//import static com.qtpselenium.test.DriverScript.currentTestDataSetID;






















import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
//import java.io.File;
//import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;




























import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.SystemClock;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.seleniumemulation.GetText;

import com.RS_BusinessRules.test.Constants;


//import com.qtpselenium.xls.read.Xls_Reader;
public class Keywords {
	
	public WebDriver driver;
	
	String CurrentURL;
	public String openBrowser(String object,String data){		
		APP_LOGS.debug("Opening browser");
		//if(data.equals("Mozilla"))
		if(CONFIG.getProperty(data).equals("Mozilla"))
			driver=new FirefoxDriver();
	
		else if(CONFIG.getProperty(data).equals("IE"))
			driver=new InternetExplorerDriver();
		else if(CONFIG.getProperty(data).equals("Chrome"))
			driver=new ChromeDriver();
		
		long implicitWaitTime=Long.parseLong(CONFIG.getProperty("implicitwait"));
		driver.manage().timeouts().implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
		return Constants.KEYWORD_PASS;

	}
	
	public String navigate(String object,String data){		
		APP_LOGS.debug("Navigating to URL");
		try{
		driver.navigate().to(CONFIG.getProperty(data));
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to navigate";
		}
		return Constants.KEYWORD_PASS;
	}
	
	public String clickLink(String object,String data){
        APP_LOGS.debug("Clicking on link ");
        try{
        driver.findElement(By.xpath(OR.getProperty(object))).click();
        }catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link"+e.getMessage();
        }
     
		return Constants.KEYWORD_PASS;
	}

	
	public  String writeInInputFromColById(String object,String data){
		APP_LOGS.debug("Writing in text box");
		try{
			driver.findElement(By.id(OR.getProperty(object))).sendKeys(data);
			}
			catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
			}
		return Constants.KEYWORD_PASS;
		
		
	}
	
	public String verifyTextById(String object, String data){
		APP_LOGS.debug("Verifying the text");
		try{
			String actual=driver.findElement(By.id(OR.getProperty(object))).getText();
			String expected=data;
	    	if(actual.equals(expected))
	    		return Constants.KEYWORD_PASS;
	    	else
	    		return Constants.KEYWORD_FAIL+"<-->text not verified"+"--actualText->>"+actual+"--expectedText->>"+expected;
			}catch(Exception e){
				return Constants.KEYWORD_FAIL+" Object not found "+e.getMessage();
			}
	}
	
	public String verifyTextByXpath(String object, String data){
		APP_LOGS.debug("Verifying the text");
		try{
			String actual=driver.findElement(By.xpath(OR.getProperty(object))).getText();
			String expected=data;
	    	    	    	
	    	if(actual.equals(expected))
	    		return Constants.KEYWORD_PASS;
	    	else
	    		return Constants.KEYWORD_FAIL+"<-->text not verified"+"--actual->>"+actual+"--expected->>"+expected;
			}catch(Exception e){
				return Constants.KEYWORD_FAIL+" Object not found "+e.getMessage();
				}
	}
		
		public String verifyTextContainedByXpath(String object, String data){
			APP_LOGS.debug("Verifying the text");
			try{
				String actual=driver.findElement(By.xpath(OR.getProperty(object))).getText();
				String expected=data;
		    	    	    	
		    	if(actual.contains(expected))
		    		return Constants.KEYWORD_PASS;
		    	else
		    		return Constants.KEYWORD_FAIL+"<-->The test is not conatined in the link"+"--actual->>"+actual+"--expected->>"+expected;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+" Object not found "+e.getMessage();
				}
	}
	
		public  String writeAccountNo(String object, String data){
		       APP_LOGS.debug("Verifying title");
		       try{
		    	   String str;
		    	   Constants.AccountNoReferee=String.valueOf((int)(1000+ Math.random()*8999));
		    	   Constants.Pin=String.valueOf((int)(10000+ Math.random()*89999));
		    	   str = data+"|"+Constants.AccountNoReferee+"|"+Constants.Pin;
		    	   Constants.MatchKey=str;
		    	   driver.findElement(By.name(OR.getProperty(object))).sendKeys(str);
		    	   }
		       	catch(Exception e){
				return Constants.KEYWORD_FAIL + " Unable to write " + e.getMessage();
			}return Constants.KEYWORD_PASS;		
		}
	
		
		
	public  String verifyTitle(String object, String data){
	       APP_LOGS.debug("Verifying title");
	       try{
	    	   String actualTitle= driver.getTitle();
	    	   String expectedTitle=OR.getProperty(object);
	    	   if(actualTitle.equals(expectedTitle))
		    		return Constants.KEYWORD_PASS;
		    	else
		    		return Constants.KEYWORD_FAIL+" -- Title not verified "+expectedTitle+" -- "+actualTitle;
			   }catch(Exception e){
					return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();

			}		
	}
	
	public  String waitTillTextVisibilityByID(String object,String data){
		  APP_LOGS.debug("Waiting for an element to be visible");
		  try{
		   		  String buf=driver.findElement(By.id(OR.getProperty(object))).getText(); 
		   		  int k=0;
		   		  while(buf!=null && k<600)
		   		  {
		   			  Thread.sleep(100L);
		   			  k=k+1;
		   		  }
		  }
		  catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to wait "+e.getMessage();
		  }
		return Constants.KEYWORD_PASS;
	}
	
	
	public  String writeInInputByXpath(String object,String data){
		APP_LOGS.debug("Writing in text box");
		
		try{
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(data);
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();

		}
		return Constants.KEYWORD_PASS;
		
	}

	public  String pause(String object,String data){
		APP_LOGS.debug("pause");
		
		try{
			Thread.sleep(5000L);
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to wait "+e.getMessage();

		}
		return Constants.KEYWORD_PASS;
		
	}
	
	public  String pauseForNsec(String object,String data){
		APP_LOGS.debug("pause");
		
		try{
			int k = Integer.parseInt(data.split("k")[0]);
			Thread.sleep((long)(k*1000));
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to wait "+e.getMessage();

		}
		return Constants.KEYWORD_PASS;
		
	}
	
	public  String closeBrowser(String object, String data){
		APP_LOGS.debug("Closing the browser");
		try{
			driver.quit();
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+"Unable to close browser. Check if its open"+e.getMessage();
		}
		return Constants.KEYWORD_PASS;

	}
	
	 
	 public String switchToDefaultContent(String object,String data){
	        APP_LOGS.debug("Switching to default content in HTML");
	        try{
	        	driver.switchTo().defaultContent(); 
	        }
	        catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Unable to switch to default content"+e.getMessage();
	        }
	        return Constants.KEYWORD_PASS;
			
		}

	 ///// Selecting I frames - Use only for //////
	public String selectFrameByName(String object,String data){
        APP_LOGS.debug("Select an Iframe with dynamically generated ID ");
        try{
        	WebElement abc = driver.findElement(By.name(data));
        	driver.switchTo().frame(abc);
        	  
            //driver.switchTo().frame(driver.findElement(By.xpath(object)));

        }
        catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
        }
        return Constants.KEYWORD_PASS;
		
	}
	
	public String selectFrame(String object,String data){
        APP_LOGS.debug("Select an Iframe with dynamically generated ID ");
        try{
        	String abc = driver.findElement(By.tagName("iframe")).getAttribute("id");
        	driver.switchTo().frame(abc);
        	  
            //driver.switchTo().frame(driver.findElement(By.xpath(object)));

        }
        catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
        }
        return Constants.KEYWORD_PASS;
		
	}
	public  String writeRandomAcntNo(String object,String data){
		  APP_LOGS.debug("Writing random integer in text box for last 4 digits of account no");
		  
		  try{
			  String randNumber=Integer.toString((int)(1000+(Math.random()*8999))); 
			  driver.findElement(By.id(OR.getProperty(object))).click();
			  driver.findElement(By.id(OR.getProperty(object))).sendKeys(randNumber);
			  Constants.AccountNo = randNumber;
			  return Constants.KEYWORD_PASS;
		  }catch(Exception e){
		   return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		  }		  
	}
	
	public  String writeRandomPin(String object,String data){
		  APP_LOGS.debug("Writing random integer in text box for  the pincode of the referrer/referee");
		  try{
			  String randNumber=Integer.toString((int)(10000+(Math.random()*89999))); 
			  driver.findElement(By.id(OR.getProperty(object))).click();
			  driver.findElement(By.id(OR.getProperty(object))).sendKeys(randNumber);
			  Constants.Pin = randNumber;
			  return Constants.KEYWORD_PASS;
		  }catch(Exception e){
		   return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		  }		  
	}
	/*
	 public String selectFrame(String object, String data) throws IOException {
		   APP_LOGS.debug("switching in iframe");
		  
		   try{
		    driver.switchTo().frame(driver.findElement(By.xpath(object)));
		   return Constants.KEYWORD_PASS;
		   }
		   catch(Exception e){
		    return Constants.KEYWORD_FAIL +"not able to switch" +e.getMessage();
		   }
		   
		  }  
	*/
	 public  String changeFocusUp(String object,String data){
		  APP_LOGS.debug("Changing the focus to top window ");
		  
		  try{
		   // considering that there is only one tab opened in that point.
		      String oldTab = driver.getWindowHandle();
		      driver.findElement(By.xpath(OR.getProperty(object))).click();
		      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		      newTab.remove(oldTab);
		      // change focus to new tab
		      driver.switchTo().window(newTab.get( 0 ) );
		      Constants.OldTab=oldTab; 
		      /*
		      Do what you want here, you are in the new tab
		      driver.close();
		      change focus back to old tab
		      driver.switchTo().window(oldTab);
		   */
		  }
		  catch(Exception e){
		   return Constants.KEYWORD_FAIL+" Unable to changefocus - "+e.getMessage();
		   }
		  return Constants.KEYWORD_PASS;
		}
	 
	 public String SwitchToWindow(String object, String data){
		   APP_LOGS.debug("Switching to new window");
		   try{ 
		  String Parenthandle=driver.getWindowHandle();
		  APP_LOGS.debug("ParentWindowHandle is" + Parenthandle );
		  APP_LOGS.debug("Driver is in the following url" +driver.getCurrentUrl());
		  Set<String> HandlesBeforeClick=driver.getWindowHandles();
		  int CurrentNoOfHandle=HandlesBeforeClick.size();
		   driver.findElement(By.xpath(OR.getProperty(object))).click();
		   APP_LOGS.debug("Button is clicked");
		   
		   waitTillNoOfWindows(CurrentNoOfHandle+1);
		   Set<String> handle=driver.getWindowHandles();
		   APP_LOGS.debug("total number of handles= "+ handle);
		   
		   if(handle.contains(Parenthandle))
		   handle.remove(Parenthandle);
		   
		   APP_LOGS.debug("no. of handles after removing parent handle"+ handle);
		   for(String winHandle : handle){  
		    APP_LOGS.debug("window handle after"+ winHandle );
		    driver.switchTo().window(winHandle);
		    APP_LOGS.debug("Second window handle="+ winHandle);
		   
		  }



		     APP_LOGS.debug("Switch to window is successful");
		   }
		  catch(Exception e){
		  
		   APP_LOGS.debug("exception caught :  "+e);
		    }
		    return Constants.KEYWORD_PASS;
		  
		  }
		  
		  public String waitTillNoOfWindows(final int numberOfWindows){
		   APP_LOGS.debug("Waiting till number of windows becomes 2");
		   try{
		     new WebDriverWait(driver,30){}.until(new ExpectedCondition<Boolean>()
		                      {
		             @Override public Boolean apply(WebDriver driver)
		                           {                       
		                 return (driver.getWindowHandles().size() == numberOfWindows);
		                           }
		                     });
		   }
		   catch(Exception e){
		    return Constants.KEYWORD_FAIL+"Number of windows is not equal to 2"+e.getMessage();
		   }
		   return Constants.KEYWORD_PASS;
		  }
	 
	 
	 public  String changeFocusDown(String object,String data){
		  APP_LOGS.debug("Changing the focus to top window ");
		  try{
			  driver.close();
		      driver.switchTo().window(Constants.OldTab);
		      
		  }
		  catch(Exception e){
		   return Constants.KEYWORD_FAIL+" Unable to changefocus - "+e.getMessage();
		   }
		  return Constants.KEYWORD_PASS;
		}
	 
	public  String writeRandomFilename(String object,String data){
		APP_LOGS.debug("Writing in text box");
		try{
			String str1= data.split("cc")[0] + Integer.toString((int)(Math.random()*100000)) + data.split("cc")[1];
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(str1);
			Constants.Filename=str1;
		}
		catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		}
		return Constants.KEYWORD_PASS;	
	}
	
	public  String writeRandomStringById(String object,String data){
		APP_LOGS.debug("Writing in text box");
		try{
			String str= data + Integer.toString((int)(Math.random()*10000));
			driver.findElement(By.id(OR	.getProperty(object))).sendKeys(str);
		}
		catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		}
		return Constants.KEYWORD_PASS;	
	}
	
	public  String generateRandomEmail(String object,String data){
		APP_LOGS.debug("Writing in text box");
		
		try{
			String email="test"+Integer.toString((int)(Math.random()*1000))+"@test"+Integer.toString((int)(Math.random()*1000));
			driver.findElement(By.id(OR	.getProperty(object))).sendKeys(email);
			Constants.Referrer=email;			
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		}
		return Constants.KEYWORD_PASS;	
	}
	public  String generateRandomRefereeEmail(String object,String data){
		APP_LOGS.debug("Writing in text box");
		
		try{
			String email="test"+Integer.toString((int)(Math.random()*1000))+"@test"+Integer.toString((int)(Math.random()*1000));
			driver.findElement(By.id(OR	.getProperty(object))).sendKeys(email);
			Constants.Referee=email;			
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		}
		return Constants.KEYWORD_PASS;	
	}
	
	public  String searchInMemberLookup(String object,String data){
		APP_LOGS.debug("Writing in text box");
		
		try{
			driver.findElement(By.id(OR	.getProperty(object))).sendKeys(Constants.Referrer);
				
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		}
		return Constants.KEYWORD_PASS;	
	}

	public  String DataToBeWritteninFile(File Fname,RandomAccessFile x){
		APP_LOGS.debug("Writing in file");
		
		try{
			RandomAccessFile raf=x;
			File file=Fname;
			//File file = new File("D:\\testRun.txt");
			String pattern = "dd/MM/yyyy:HH:mm";
	       SimpleDateFormat format = new SimpleDateFormat(pattern);
	       // formatting
	       String date=format.format(new Date());
	       System.out.println("current date:"+date);
	     
	       long fileLength = file.length();
       	//RandomAccessFile raf = new RandomAccessFile(file, "rw");
       	raf.seek(fileLength+1);
    	//raf.writeChars("\r\n");
    	raf.writeBytes(System.getProperty("line.separator"));
    	raf.writeBytes(date);
    	raf.writeBytes("-----");
        raf.writeBytes(Constants.Referrer);
        raf.close();
        
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		}
		return Constants.KEYWORD_PASS;	
	}
	
	public String writeCSVData(String object,String data){
        APP_LOGS.debug("Write Data to a file or append data to a file");
        try{
        	if(Constants.ActualText_Valid.equalsIgnoreCase("Valid"))
        	{
        	File file = new File(data);
        	RandomAccessFile raf = new RandomAccessFile(file, "rw");
        	if(file.exists())
        	{
        		System.out.println("file exists");
        		raf.seek(0);
        		if(raf.readLine()==null)
        		raf.writeBytes("  Time Stamp       -----   Valid Referrers");
        		else 
        			System.out.println("Title already present");
        		DataToBeWritteninFile(file,raf);
        	}
        	
        }
        
        	else if(Constants.ActualText_Unknown.equalsIgnoreCase("Unknown"))
        	{
        		File file = new File(data);
            	RandomAccessFile raf = new RandomAccessFile(file, "rw");
            	if(file.exists())
            	{
            		System.out.println("file exists");
            		raf.seek(0);
            		if(raf.readLine()==null)
            		raf.writeBytes("  Time Stamp       -----  Invalid Referrers");
            		else 
            			System.out.println("Title already present");
            		DataToBeWritteninFile(file,raf);
            	}
        	}
        		
       else
        	System.out.println("Referrer's stats is not Valid hence not abble to add artifacts in file");
        
        }
        	catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Encountered unexpected error while writing data to file "+e.getMessage();
        }
     
		return Constants.KEYWORD_PASS;
	}
	
	public  String checkCheckBox(String object,String data){
		APP_LOGS.debug("Checking checkbox");
		try{
			// true or null
			String checked=driver.findElement(By.xpath(OR.getProperty(object))).getAttribute("checked");
			if(checked==null)// checkbox is unchecked
				driver.findElement(By.xpath(OR.getProperty(object))).click();
			   Thread.sleep(1000L);
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" - Could not find checkbox";
		}
		return Constants.KEYWORD_PASS;
	}	
	
	
//***********************************Click Link Types*******************************************************************//
	public String importFilenameIsEnabled(String object,String data){
        APP_LOGS.debug("Clicking on link by content");
        try{
        	if(driver.findElement(By.partialLinkText(Constants.Filename)).isEnabled() == false)
        	{
        	return Constants.KEYWORD_FAIL+" -- Not able to click on link ";
        	}
        	
        }catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
        }
    	return Constants.KEYWORD_PASS;
	}
	
	public String checkIfWebElementEnabledbyXpath(String object,String data){
        APP_LOGS.debug("Clicking on link by content");
        try{
        	if(driver.findElement(By.xpath(OR.getProperty(object))).isEnabled() == false)
        	{
        	return Constants.KEYWORD_FAIL+" -- Not able to click on link -- account is not yet validated";
        	}
        	
        }catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
        }
    	return Constants.KEYWORD_PASS;
	}
	public String exportFilenameIsEnabled(String object,String data){
        APP_LOGS.debug("Clicking on link by content");
        try{
        	if(driver.findElement(By.partialLinkText(data)).isEnabled() == false)
        	{
        	return Constants.KEYWORD_FAIL+" -- Not able to click on link ";
        	}
        	
        }catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
        }
    	return Constants.KEYWORD_PASS;
	}
	
	 public  String VerifyTextOfLastColumnOfLastRow(String object,String data){
     APP_LOGS.debug("Clicking on last column element of last row of a table");
     try{
      List<WebElement> TableRow=driver.findElements(By.xpath("//*[@class='record_list']/tbody/tr"));
      
      System.out.println("Size of table is: "+TableRow.size());
      int NumOfRows=TableRow.size();
      int LastRow=NumOfRows-1;
      for(int i = 0;i<TableRow.size();i++)
      {
      }
      List<WebElement> LastRowCol=TableRow.get(LastRow).findElements(By.tagName("td"));
     
      System.out.println("Size of table is: "+LastRowCol.size());
      int NumOfCol=LastRowCol.size(); 
      int currentRow=0;
      int HeaderRow;
        for(int i=0; i<LastRow;i++)
       { List<WebElement> RefereeMailElement=TableRow.get(i).findElements(By.tagName("td"));
    
      String RefereeMail=TableRow.get(i).findElements(By.tagName("td")).get(NumOfCol-2).getText();
      
       if((Constants.Referee).equals(RefereeMail))
       
        {
         System.out.println("referee mail matches the expected mail");
        currentRow=i;
        
        }
        
        else
         //i++;
         LastRow-=1; 
         
      }
      System.out.println("current row index is:"+currentRow);
      WebElement ReferralStatus=TableRow.get(currentRow).findElements(By.tagName("td")).get(NumOfCol-1);
      String Actual=ReferralStatus.getText();
     
      System.out.println("Actual text is:"+ Actual);
      System.out.println("Expected text is: "+data);
      if(Actual.equalsIgnoreCase(data))
       APP_LOGS.debug("Actual text is equal to expected text");
      else
       APP_LOGS.debug("Actual text is not equal to expected text");
		}
		 
		        return Constants.KEYWORD_PASS;
		   }catch(Exception e){
		    return Constants.KEYWORD_FAIL+"unable to wait for frame"+e.getMessage();
		   }
		   
		   
		  }
	 
	public String writeDataInCSV(String object,String data) {
	    try {
	       FileWriter writer = new FileWriter(data);
	       writer.write("Account Number");
	       writer.write("lname|"+Constants.AccountNo+"|"+Constants.Pin);
	       writer.write('\n');
	       writer.write("EmailID : ");
		   writer.write(',');
	       writer.write(Constants.Referrer);
	       writer.flush();
           writer.close();
	       return Constants.KEYWORD_PASS;
	       } catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
	        }
	}
	
	public String writeDataInCSVReferee(String object,String data) {
	    try {
	       FileWriter writer = new FileWriter(data);
	       writer.write("Account Number");
	       writer.write("lname|"+Constants.AccountNo+"|"+Constants.Pin);
	       writer.write('\n');
	       writer.write("EmailID : ");
		   writer.write(',');
	       writer.write(Constants.Referee);
	       writer.flush();
           writer.close();
	       return Constants.KEYWORD_PASS;
	       } catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
	        }
	}
	
	public String writeImportCSVReferee(String object,String data) {
	    try {
	       FileWriter writer = new FileWriter("D:\\tester1referee.csv");
	       writer.write("Account Number");
	       writer.write('\n');
		   writer.write(Constants.MatchKey);
	       writer.write(',');
	       String pattern = "MM/dd/yyyy";
	       SimpleDateFormat format = new SimpleDateFormat(pattern);
	       // formatting
	       String date=format.format(new Date());
	       writer.write(date);
	       writer.write(',');
	       writer.write(data.split(",")[0]);
	       for(int i=1;i<=12;i++){ 
	       writer.write(',');
	       }
	       writer.write(data.split(",")[1]);
	       writer.write(',');
	       writer.write(data.split(",")[2]);
	       writer.flush();
	       writer.close();
	       
	       return Constants.KEYWORD_PASS;
	       } catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
	        }
	}
	
	public String writeImportCSV(String object,String data) {
	    try {
	       FileWriter writer = new FileWriter("D:\\tester1.csv");
	       writer.write("Account Number");
	       writer.write('\n');
		   writer.write("lname|"+Constants.AccountNo+"|"+Constants.Pin);
	       writer.write(',');
	       String pattern = "MMddyyyy";
	       SimpleDateFormat format = new SimpleDateFormat(pattern);
	       // formatting
	       String date=format.format(new Date());
	       System.out.println(date);
	       String month = date.substring(0,2);
	       String day = date.substring(2,4);
	       String year = date.substring(4,8);
	       String systemDate=month+"/"+day+"/"+year;
	       writer.write(systemDate);
	       writer.write(',');
	       writer.write(data);      
	       writer.flush();
	       writer.close();
	       
	       return Constants.KEYWORD_PASS;
	       } catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
	        }
	}
	
	public String clickWebElementById(String object,String data){
        APP_LOGS.debug("Clicking on link by id");
        try{
        driver.findElement(By.id(OR.getProperty(object))).click();
        }catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
        }
     
		return Constants.KEYWORD_PASS;
	}
	
	public String clickWebElemetByXpath(String object,String data){
        APP_LOGS.debug("Clicking on link by xpath");
        try{
        driver.findElement(By.xpath(OR.getProperty(object))).click();
        }catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
        }
     
		return Constants.KEYWORD_PASS;
	}
	
	
	
	 public String clickWebElemetByLink_TextNum(String object,String data){
	 
        APP_LOGS.debug("Clicking on link by link text");
        try{
        	String p[]=data.split("k");
        	
			driver.findElement(By.partialLinkText(p[0])).click();
        	}
        catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
        	}
     
		return Constants.KEYWORD_PASS;
	}
	 
	 public String clickWebElemetByLink_Text(String object,String data){
		 
	        APP_LOGS.debug("Clicking on link by link text");
	        try{
				driver.findElement(By.partialLinkText(data)).click();
	        	}
	        catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
	        	}
	     
			return Constants.KEYWORD_PASS;
		}
	 public String writeEmailForSearch(String object,String data){
		 
	        APP_LOGS.debug("Clicking on link by link text");
	        try{
				driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(Constants.Referrer);
	        	}
	        catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
	        	}
	     
			return Constants.KEYWORD_PASS;
		}
	 
	 public String writeEmailForSearchReferee(String object,String data){
		 
	        APP_LOGS.debug("Clicking on link by link text");
	        try{
				driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(Constants.Referee);
	        	}
	        catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
	        	}
	     
			return Constants.KEYWORD_PASS;
		}
	 
	 public  String clickEmailAndchangeFocusNewWindow(String object,String data){
		  APP_LOGS.debug("Changing the focus to top window ");
		  
		  try{
		   // considering that there is only one tab opened in that point.
		      String oldTab = driver.getWindowHandle();
		      driver.findElement(By.partialLinkText(Constants.Referrer)).click();
		      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		      System.out.println(oldTab);
		      newTab.remove(oldTab);
		      System.out.println(newTab);
		      // change focus to new tab
		      driver.switchTo().window(newTab.get(0));
		      System.out.println("no Exceptions encountered");
		     
		  }	  catch(Exception e){
			   return Constants.KEYWORD_FAIL+" Unable to changefocus - "+e.getMessage();
			   }
			  return Constants.KEYWORD_PASS;
		}		  
		  public  String clickEmailAndchangeFocusNewWindowReferee(String object,String data){
			  APP_LOGS.debug("Changing the focus to top window ");
			  
			  try{
			   // considering that there is only one tab opened in that point.
			      String oldTab = driver.getWindowHandle();
			      
			      ArrayList<String> HandlesBeforeClick=new ArrayList<String>(driver.getWindowHandles());
			      System.out.println("No. of windows handle before click:"+HandlesBeforeClick.size());
			      System.out.println(" windows handles before click without set:"+driver.getWindowHandles() );
			      int CurrentNoOfHandle=HandlesBeforeClick.size();
			      
			      driver.findElement(By.partialLinkText(Constants.Referee)).click();
			      waitTillNoOfWindows(CurrentNoOfHandle+1);
			      ArrayList<String> HandlesAfterClick =new ArrayList<String>(driver.getWindowHandles());
			      System.out.println("Number of handles after click: "+HandlesAfterClick.size());
			      System.out.println("Handles after click with set"+HandlesAfterClick);
			      System.out.println("Number of handles after click without hashset:"+driver.getWindowHandles());
			     /*String NewHandle= null;
			      for(int i=0;i<=HandlesAfterClick.size();i++)
			      {
			    	  if(HandlesBeforeClick.contains(HandlesAfterClick.get(i)))
			    		  System.out.println("HandlesAfterClick contains the element in HandlesBeforeClick  ");
			    	  else
			    		  NewHandle=HandlesAfterClick.get(i);
			    	  System.out.println("New handle is:"+NewHandle);
			      }*/
			    /*
			      HandlesAfterClick.removeAll(HandlesBeforeClick);
			      System.out.println("Number of handles after removing handles:"+HandlesAfterClick.size());
			      System.out.println("Handles after removing all reduntant handles:"+HandlesAfterClick);
			      
			      System.out.println("Total number of windows handles:"+HandlesAfterClick);
			      System.out.println("Old window's handle:");
			      System.out.println(oldTab);*/
			     /* HandlesAfterClick.remove(oldTab);
			      System.out.println("New window's handle:");
			      System.out.println(HandlesAfterClick);
			      String NewTab=null;
			      
			    NewTab=HandlesAfterClick.toString();
			      System.out.println("handles after click after converting it to string:"+NewTab);
			      Thread.sleep(4000L);
			      // change focus to new tab
			      driver.switchTo().window(NewTab);*/
			      if(HandlesAfterClick.contains(oldTab))
			    	  HandlesAfterClick.remove(oldTab);
					   
					   APP_LOGS.debug("no. of handles after removing parent handle"+ HandlesAfterClick);
					   for(String winHandle : HandlesAfterClick){  
					    APP_LOGS.debug("window handle after"+ winHandle );
					    driver.switchTo().window(winHandle);
					    APP_LOGS.debug("Second window handle="+ winHandle);
			      System.out.println("no Exceptions encountered");
			     
					   }
			  }
			  
		  catch(Exception e){
		   return Constants.KEYWORD_FAIL+" Unable to changefocus - "+e.getMessage();
		   }
		  return Constants.KEYWORD_PASS;
	}
		  
		  public String writeRefereeImportCSV(String object,String data) {
			    try { APP_LOGS.debug("Writing referee import csv");
			    	String[] p=data.split(",");
			       FileWriter writer = new FileWriter("D:\\referee.csv");
			       writer.write("Account Number");
			       writer.write('\n');
				   writer.write(Constants.MatchKey);
				   System.out.println("referee account no. as writtn in csv:" +Constants.MatchKey);
			       writer.write(',');
			       String pattern = "MM/dd/yyyy";
			       SimpleDateFormat format = new SimpleDateFormat(pattern);
			       // formatting
			       String date=format.format(new Date());
			       System.out.println("current date:"+date);
			       /*String month = date.substring(0,2);
			       String day = date.substring(2,4);O
			       String year = date.substring(4,8);
			       String systemDate=month+"/"+day+"/"+year;
			       writer.write(systemDate);*/
			       Calendar cal = Calendar.getInstance();
			        cal.add(Calendar.DATE, Integer.parseInt(p[0]));
			        Date NewDate = cal.getTime();    
			        String ModifiedDate = format.format(NewDate);
			        System.out.println("Modified date is: "+ModifiedDate);
			       writer.write(ModifiedDate);
			       writer.write(',');
			       writer.write(p[1]);  
			       for(int i=3;i<15;i++)
			    	   writer.write(',');  
			       writer.write(p[2]);
			       writer.write(',');
			       writer.write(p[3]);
			       System.out.println("product and value is: "+p[2]+p[3]);
			       writer.flush();
			       writer.close();
			       
			       return Constants.KEYWORD_PASS;
			       } catch(Exception e){
						return Constants.KEYWORD_FAIL+" -- Unable to write file "+e.getMessage();
			        }
			}
		  public  String SetQATime(String object,String data){
				APP_LOGS.debug("Setting QA time of the desired site");
				
				try{
					/*
					long currentTime=System.currentTimeMillis();
					System.out.println("current system time is: "+currentTime);
					
					
					String[] p=data.split(",");
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL+ "n");
				
					driver.findElement(By.linkText("http://test1-rddev.staging.sparkrefer.com/controlcenter")).sendKeys(selectLinkOpeninNewTab);
					String pattern = "yyyy/MM/dd";
				       SimpleDateFormat format = new SimpleDateFormat(pattern);
				       // formatting
				       String date=format.format(new Date());
				       System.out.println("current date:"+date);
				       String month = date.substring(0,2);
				       String day = date.substring(2,4);O
				       String year = date.substring(4,8);
				       String systemDate=month+"/"+day+"/"+year;
				       writer.write(systemDate);
				       Calendar cal = Calendar.getInstance();
				       System.out.println("date in p[1] is "+ p[1]);
				        cal.add(Calendar.DATE, Integer.parseInt(p[1]));
				        Date NewDate = cal.getTime();    
				        String ModifiedDate = format.format(NewDate);
				        System.out.println("date to be input: "+p[0]+ModifiedDate+p[2]);*/
				        
			Robot robot= new Robot();
					robot.keyPress(KeyEvent.VK_CONTROL);
					robot.keyPress(KeyEvent.VK_T);
					robot.keyRelease(KeyEvent.VK_T);
					robot.keyRelease(KeyEvent.VK_CONTROL);
					driver.get("http://test1-rddev.staging.sparkrefer.com/controlcenter/qaTime.jsp?action=set&time=2014/05/30:01:01");
					//ActionChains(driver).send_keys(Keys.COMMAND, "t").perform()
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+" Unable to open page "+e.getMessage();

				}
				return Constants.KEYWORD_PASS;
				
			}
		  public  String SetQATimeGeneralized(String object,String data){
				APP_LOGS.debug("Setting QA time of the desired site");
				
				try{
					Robot robot= new Robot();
					robot.keyPress(KeyEvent.VK_CONTROL);
					robot.keyPress(KeyEvent.VK_T);
					robot.keyRelease(KeyEvent.VK_T);
					robot.keyRelease(KeyEvent.VK_CONTROL);
					String[] p=data.split(",");
					System.out.println("content in p[0] is: "+p[0]);
					System.out.println("content in p[1] is: "+p[1]);
					
					String pattern = "yyyy/MM/dd:HH:mm";
				       SimpleDateFormat format = new SimpleDateFormat(pattern);
				       // formatting
				       String date=format.format(new Date());
				       System.out.println("current date:"+date);
				     
				       Calendar cal = Calendar.getInstance();
				       System.out.println("current Date shown by calender: "+Calendar.DATE);
				        cal.add(Calendar.DATE, Integer.parseInt(p[1]));
				        Date NewDate = cal.getTime();    
				        String ModifiedDate = format.format(NewDate);
				        System.out.println("data to be input in URL: "+p[0]+ModifiedDate);
				        

					
					driver.get(p[0]+ModifiedDate);
					//ActionChains(driver).send_keys(Keys.COMMAND, "t").perform()
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+" Unable to open page "+e.getMessage();

				}
				return Constants.KEYWORD_PASS;
				
			}
		  public  String OpenWebpageInNewTab(String object,String data){
				APP_LOGS.debug("Opening webpage in new tab");
				
				try{
						
					/*String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL+ "n");
				
					driver.findElement(By.linkText("http://test1-rddev.staging.sparkrefer.com/controlcenter")).sendKeys(selectLinkOpeninNewTab);*/
					
				      
			Robot robot= new Robot();
					robot.keyPress(KeyEvent.VK_CONTROL);
					robot.keyPress(KeyEvent.VK_T);
					robot.keyRelease(KeyEvent.VK_T);
					robot.keyRelease(KeyEvent.VK_CONTROL);
					//Thread.sleep(6000L);
					driver.get(data);
					//ActionChains(driver).send_keys(Keys.COMMAND, "t").perform()
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+" Unable to open page "+e.getMessage();

				}
				return Constants.KEYWORD_PASS;
				
			}
		  
//**********************************Keywords created by Ankit*******************************************************
		       public  String WriteAccountInformation(String object, String data){
		       APP_LOGS.debug("Writing account information to different fields and saving them to a constant");
		       try{
		    	   String str;
		    	   Constants.AccountNoReferee=String.valueOf((int)(1000+ Math.random()*8999));
		    	   Constants.Pin=String.valueOf((int)(10000+ Math.random()*89999));
		    	   driver.findElement(By.id("profile-mk1")).sendKeys(data);
		    	   driver.findElement(By.id("profile-mk2")).sendKeys(Constants.AccountNoReferee);
		    	   driver.findElement(By.id("profile-mk3")).sendKeys(Constants.Pin);
		    	   
		    	   str = data+"|"+Constants.AccountNoReferee+"|"+Constants.Pin;
		    	   Constants.MatchKey=str;
		    	   }
		       	catch(Exception e){
		       		System.out.println("Inside Catch block");
		       		try{
		       		System.out.println("Inside inner try block of catch");
		       	 String str;
		    	   Constants.AccountNoReferee=String.valueOf((int)(1000+ Math.random()*8999));
		    	   Constants.Pin=String.valueOf((int)(10000+ Math.random()*89999));
		    	   str = data+"|"+Constants.AccountNoReferee+"|"+Constants.Pin;
		    	   System.out.println("data to be entered in Account number is: "+str);
		    	   driver.findElement(By.name(OR.getProperty(object))).sendKeys(str);
		    	   Constants.MatchKey=str;
		       		}
		       		catch(Exception E){
		       			System.out.println("Inside inner catch of catch");
				return Constants.KEYWORD_FAIL + " Unable to write " + E.getMessage();
			
		            }
		       	}return Constants.KEYWORD_PASS;		
		}
		  
		  public String writeRefereeImportCSVNoPurchaseMade(String object,String data) {
			    try {
			    	String[] p=data.split(",");
			       FileWriter writer = new FileWriter("D:\\referee.csv");
			       writer.write("Account Number");
			       writer.write('\n');
				   writer.write(Constants.MatchKey);
				   System.out.println("referee account no. as writtn in csv:" +Constants.MatchKey);
			       writer.write(',');
			       /*String pattern = "MM/dd/yyyy";
			       SimpleDateFormat format = new SimpleDateFormat(pattern);
			       // formatting
			       String date=format.format(new Date());
			       System.out.println("current date:"+date);
			       String month = date.substring(0,2);
			       String day = date.substring(2,4);O
			       String year = date.substring(4,8);
			       String systemDate=month+"/"+day+"/"+year;
			       writer.write(systemDate);
			       Calendar cal = Calendar.getInstance();
			        cal.add(Calendar.DATE, Integer.parseInt(p[0]));
			        Date NewDate = cal.getTime();    
			        String ModifiedDate = format.format(NewDate);
			        System.out.println("Modified date is: "+ModifiedDate);
			       writer.write(ModifiedDate);*/
			       writer.write(',');
			       writer.write(p[0]);  
			      /* for(int i=3;i<15;i++)
			    	   writer.write(',');  
			       writer.write(p[2]);
			       writer.write(',');
			       writer.write(p[3]);
			       System.out.println("product and value is: "+p[2]+p[3]);*/
			       writer.flush();
			       writer.close();
			       
			       return Constants.KEYWORD_PASS;
			       } catch(Exception e){
						return Constants.KEYWORD_FAIL+" -- Unable to write file "+e.getMessage();
			        }
			}
		  
		  public  String waitTillPageRefreshed(String object,String data){
				APP_LOGS.debug("Waiting for page to refresh");
				try{
					ExpectedCondition<Boolean> expected=new ExpectedCondition<Boolean>(){
						@Override
		                public Boolean apply(WebDriver driver) {
							return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
						}
					};
					WebDriverWait wait=new WebDriverWait(driver,60);
					
					wait.until(expected);
						
							
					return Constants.KEYWORD_PASS;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+"unable to wait for frame"+e.getMessage();
				}
				
				
			}
		  
		  public  String waitTillObjecttextVisibility(String object,String data){
				APP_LOGS.debug("Waiting for an element to be visible");
				try{
					WebDriverWait wait=new WebDriverWait(driver,25);
					wait.until(ExpectedConditions.textToBePresentInElementValue(By.xpath(OR.getProperty(object)), data));
					return Constants.KEYWORD_PASS;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+"unable to wait for element"+e.getMessage();
				}
			}
		  
		  public  String waitTillFrameVisibilityAndSwitchToIt(String object,String data){
				APP_LOGS.debug("Waiting for a frame to be visible");
				try{
					WebDriverWait wait=new WebDriverWait(driver,60);
					
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(OR.getProperty(object)));
					return Constants.KEYWORD_PASS;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+"unable to wait for frame"+e.getMessage();
				}
				
				
			}
		  public  String waitTillObjectVisibility(String object,String data){
				APP_LOGS.debug("Waiting for an element to be visible");
				try{
					WebDriverWait wait=new WebDriverWait(driver,60);
					
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(OR.getProperty(object))));
					return Constants.KEYWORD_PASS;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+"unable to wait for element"+e.getMessage();
				}
			}
		  public  String SaveCurrentURLinGlobalVariable(String object,String data){
				APP_LOGS.debug("Saving current url in a global variable");
				try{
					CurrentURL=driver.getCurrentUrl();
					return Constants.KEYWORD_PASS;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+"unable to open URL"+e.getMessage();
				}
			}
		  public  String OpenURLFromGlobalVariable(String object,String data){
				APP_LOGS.debug("Opening a URL saved in a global variable");
				try{
					driver.get(CurrentURL);
					return Constants.KEYWORD_PASS;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+"unable to open URL"+e.getMessage();
				}
			} 
		  
		  public  String submitform(String object,String data){
	          APP_LOGS.debug("submitting form");
	          try{
	              driver.findElement(By.id(OR.getProperty(object))).submit();
	              }catch(Exception e){
	         return Constants.KEYWORD_FAIL+" -- Not able to click on Button"+e.getMessage();
	              }
	          
	          
	    return Constants.KEYWORD_PASS;
	   }
	   
		  public String verifyTextByXpathAndSaveToConstant(String object, String data){
				APP_LOGS.debug("Verifying the text");
				try{
					String actual=driver.findElement(By.xpath(OR.getProperty(object))).getText();
					String expected=data;
			    	    	    	
			    	if(actual.equals(expected))
			    	{
			    		Constants.ActualText_Valid=actual;
			    		return Constants.KEYWORD_PASS;
			    	
			    	}	
			    		else
			    			Constants.ActualText_Unknown=actual;
			    		return Constants.KEYWORD_FAIL+"<-->text not verified"+"--actual->>"+actual+"--expected->>"+expected;
					}catch(Exception e){
						return Constants.KEYWORD_FAIL+" Object not found "+e.getMessage();
						}
			}
	   
		  public String clickMailTrapMail(String object,String data){
				 
		        APP_LOGS.debug("Clicking on mail in MailTrap");
		        try{
		 List<WebElement> EmailList =driver.findElements(By.className("highlight"));
		
	
		 System.out.println("Number of emails:"+EmailList.size());
		/* ArrayList<String> EmailTextList= new ArrayList<String>();
		 for(int i=0; i<EmailList.size(); i++){
		 EmailTextList.add(EmailList.get(i).getText());
		 }*/
				 int flag=0;
		 for(int i=0; i<EmailList.size();i++)
		 {flag=0;
			 System.out.println("Inside loop Email list is:"+EmailList.get(i).getText());
			 if(EmailList.get(i).getText().equalsIgnoreCase(Constants.Referee)){
				 System.out.println("Referee email present:"+EmailList.get(i).getText());
				 EmailList.get(i).click();
				 break;
		 }
			 else
				 System.out.println("Email not present");
				 flag=1;
		 }	
		 if(flag==1)
			 System.out.println("The required is not email found ");
		        	}
		        catch(Exception e){
					return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
		        	}
		     
				return Constants.KEYWORD_PASS;
			}
		  public String ClickElementByName(String object,String data){
		        APP_LOGS.debug("Clicking on element");
		        try{
		        	
		        	driver.findElement(By.name("commit")).click();
		        }
		        catch(Exception e){
					return Constants.KEYWORD_FAIL+" -- Unable to switch to default content"+e.getMessage();
		        }
		        return Constants.KEYWORD_PASS;
				
			}
		  public String selectFrameMailtrap(String object,String data){
		        APP_LOGS.debug("Select an Iframe with dynamically generated ID ");
		        try{
		        	String abc = driver.findElement(By.tagName("iframe")).getAttribute("src");
		        	System.out.println("mailtrap no. of handles before opening new tab:"+driver.getWindowHandles());
		        	Robot robot= new Robot();
					robot.keyPress(KeyEvent.VK_CONTROL);
					robot.keyPress(KeyEvent.VK_T);
					robot.keyRelease(KeyEvent.VK_T);
					robot.keyRelease(KeyEvent.VK_CONTROL);
					driver.get(abc);
		        	  System.out.println("No. of handles after opening in new tab:"+driver.getWindowHandles());
		            //driver.switchTo().frame(driver.findElement(By.xpath(object)));

		        }
		        catch(Exception e){
					return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
		        }
		        return Constants.KEYWORD_PASS;
		  }


public String ClickLinkinTableByVerifyingTextInNextCol(String object,String data){  
			    APP_LOGS.debug("Clicking on link");
			    try{
			    	
					 WebElement element=driver.findElement(By.className("table table-hover table-condensed"));
					 List<WebElement> rowCollection=element.findElements(By.xpath("//*[@id='referralsInVerifyingWidget']/div/table/tbody/tr"));
					 for(WebElement rowElement:rowCollection)
					     {
					     List<WebElement> colCollection=rowElement.findElements(By.tagName("td"));
					     for(WebElement colElement:colCollection)
					         {
					         if(colElement.getText().equals(Constants.RandomAccountNo))
					         {
					        	 colElement.findElement(By.linkText("open")).click();    
					             }
					         }
					     ////*[@id='referralsInVerifyingWidget']/div/table/tbody/tr/td/a
			        break;
			           }
			        
			      
			    }catch(Exception e){
			     return Constants.KEYWORD_FAIL+" -- Not able to click";
			    }
			    return Constants.KEYWORD_PASS;
			   }


public String clickWebElementByName(String object,String data){
		        APP_LOGS.debug("Clicking on link by id");
		        try{
		        driver.findElement(By.name(OR.getProperty(object))).click();
		        		
		        }catch(Exception e){
					return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
		        }
		     
				return Constants.KEYWORD_PASS;
			}

public String writePurchaseImportlineitemsCSVSalesRep(String object,String data) {
			    try {
			       FileWriter writer = new FileWriter("D:\\tester3.csv");
			       PrintWriter pw = new PrintWriter(writer);
			       
			       pw.print("Purchase Number");
			       writer.write(',');
			       pw.print("Account Number");
			       writer.write(',');
			       pw.print("Purchase Date");
			       writer.write(',');
			       pw.print("Product Code");
			       writer.write(',');
			       pw.print("Total Dollars Spent");
			       writer.write(',');
			       pw.print("Quantity");
			       writer.write(',');
			       pw.println("Unit Price");
			       //String randPurchaseNumber=Long.toString((long)(1000000000+(Math.random()*8999999999L)));
			       //Constants.RandomPurchaseNo = randPurchaseNumber;
			       writer.write(Constants.RandomPurchaseNo);
				   writer.write(',');
			       writer.write(Constants.RandomAccountNo);
			       writer.write(',');
			       String pattern = "MMddyyyy";
			       SimpleDateFormat format = new SimpleDateFormat(pattern);
			       // formatting
			       String date=format.format(new Date());
			       System.out.println(date);
			       String month = date.substring(0,2);
			       String day = date.substring(2,4);
			       String year = date.substring(4,8);
			       String systemDate=month+"/"+day+"/"+year;
			       writer.write(systemDate);
			       writer.write(',');
			       
			       writer.write(data.split(",")[0]);
			       writer.write(',');
			       writer.write(data.split(",")[1]);
			       writer.write(',');
			       writer.write(data.split(",")[2]); 
			       writer.write(',');
			       writer.write(data.split(",")[3]); 
			       writer.flush();
			       writer.close();
			       
			       return Constants.KEYWORD_PASS;
			       } catch(Exception e){
						return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
			        }
			}


public String writePurchaseImportCSVSalesRep(String object,String data) {
			    try {
			       FileWriter writer = new FileWriter("D:\\tester2.csv");
			       PrintWriter pw = new PrintWriter(writer);
			       
			       pw.print("Purchase Number");
			       writer.write(',');
			       pw.print("Account Number");
			       writer.write(',');
			       pw.print("Purchase Date");
			       writer.write(',');
			       pw.print("Product Code");
			       writer.write(',');
			       pw.println("Total Dollars Spent");
			       String randPurchaseNumber=Long.toString((long)(100000000000L+(Math.random()*899999999999L)));
			       Constants.RandomPurchaseNo = randPurchaseNumber;
			       writer.write(Constants.RandomPurchaseNo);
			       writer.write(',');
			       writer.write(Constants.RandomAccountNo);
			       writer.write(',');
			       String pattern = "MMddyyyy";
			       SimpleDateFormat format = new SimpleDateFormat(pattern);
			       // formatting
			       String date=format.format(new Date());
			       System.out.println(date);
			       String month = date.substring(0,2);
			       String day = date.substring(2,4);
			       String year = date.substring(4,8);
			       String systemDate=month+"/"+day+"/"+year;
			       writer.write(systemDate);
			       writer.write(',');
			       
			       writer.write(data.split(",")[0]);
			       writer.write(',');
			       writer.write(data.split(",")[1]);     
			       writer.flush();
			       writer.close();
			       
			       return Constants.KEYWORD_PASS;
			       } catch(Exception e){
						return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
			        }
			}


public String writeImportCSVSalesRep(String object,String data) {
			    try {
			       FileWriter writer = new FileWriter("D:\\tester1.csv");
			       writer.write("Account Number");
			       writer.write('\n');
				   writer.write(Constants.RandomAccountNo);
			       writer.write(',');
			       String pattern = "MMddyyyy";
			       SimpleDateFormat format = new SimpleDateFormat(pattern);
			       // formatting
			       String date=format.format(new Date());
			       System.out.println(date);
			       String month = date.substring(0,2);
			       String day = date.substring(2,4);
			       String year = date.substring(4,8);
			       String systemDate=month+"/"+day+"/"+year;
			       writer.write(systemDate);
			       writer.write(',');
			       
			       writer.write(data.split(",")[0]);
			       for(int i=1;i<=12;i++){ 
			       writer.write(',');
			       }
			       writer.write(data.split(",")[1]);
			       writer.write(',');
			       writer.write(data.split(",")[2]);
			       writer.flush();
			       writer.close();
			       
			       
			      
			       
			       return Constants.KEYWORD_PASS;
			       } catch(Exception e){
						return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
			        }
			}
//***********************************Click Link Types*******************************************************************//
	
	 
	
	/* 
	 * Verify if the popup is displayed by ID/xPATH 
	 * SNo - 1
	 */

}

